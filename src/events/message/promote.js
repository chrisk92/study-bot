const Raven = require('raven');

module.exports = {
  description: 'Lol promotion',
  async execute(message) {
    try {
      const {
        mentions,
        channel,
        client
      } = message;
       let mention;
      if (mentions.members) {
        mention = mentions.members // Get mentions in message
          .map(m => m.id);
      }
      await channel
        .send(
          `${mention[0] ? `<@${mention[0]}>` : ''}`, // If mention is included, add it to message
          {
            embed: {
              color: 1441536,
              author: {
                name: 'Promotion'
              },
              description: 'Congratulations on your promotion to Admin! ',
              timestamp: new Date(),
              footer: {
                icon_url: client.user.avatarURL,
                text: 'StudyBot'
              }
            }
          }
        );
      await message
        .delete();
    } catch (err) {
      Raven.captureException(err);
    }
  }
};
