const Raven = require('raven');
module.exports = {
  description: 'Sends result of ping latency',
  async execute(message) {
    try {
      const {
        channel
      } = message;
      const m1 = await channel.send('Pong');
      await channel
        .send('Pong, latency: ' + (m1.createdTimestamp - message.createdTimestamp) + ' ms');
      await m1
        .delete();
    } catch (err) {
      Raven.captureException(err);
    }
  }
};
