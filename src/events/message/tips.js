const Raven = require('raven');

module.exports = {
  description: 'Sends an embed of the tips',
  async execute(message, client) {
    try {
      const {
        channel
      } = message;

      await channel
        .send(
          '',
          {
            embed: {
              color: 15608105,
              author: {
                name: 'Tips'
              },
              description: 'Welcome to 勉強軍!',
              fields: [
                {
                  name: 'A. Getting Started',
                  value: '**1) Familiarize yourself with the rules** so you don\'t get in trouble.\n'
                    + '**2) Introduce yourself** Say hello in <#495523878429523988> and talk a little bit about yourself!\n'
                    + '**3) Get a new role** to display what language you are learning <#495586919158251521>.\n'
                    + '**4) Start a discussion or ask for help** in the respective channels.\n'
                    + '**5) If you have a question, don\'t hesitate to ask it.** To save time, post it instead of asking "Does anyone know what X means" or "Can someone help with Y?"\n'
                   },
                {
                  name: 'B. StudyBot',
                  value: '**1) Server Commands** —\n'
                    + '• `@Staff <@USER> <OFFENSE>` to submit a report.\n'
                    + '• `?ta5` macro that quickly displays \'If you have a question, don\'t hesitate to ask it. To save time, post it instead of asking "Does anyone know what X means?" or "Can someone help with Y?"\'\n'
                },
                {
                  name: 'C. Contributing',
                  value: '**1) Valued Contributor** — If you display considerable knowledge, activity and maturity in channels, you will be recognized as a valued contributor.\n'
                },

                {
                  name: 'D. Sharing the server',
                  value: 'The more people that join, the more knowledge that can be shared! Consequently, I encourage everybody to share the server with your friends!\n\nShare link: https://discord.gg/xt8VVVG \n\u200b'
                },
            /*    {
                  name: 'E. Help Mentioning',
                  value: '**1)** Use `?mention <message id of question> <role>[ <role>]` in the channel you want to mention in to create a \'key\'.\n'
                  + '**2)** You may use up to two related pings, e.g. Japanese.\n'
                  + '**3)** After 15 minutes, you may use `?mention` to redeem your key in the same channel you used the command.\n'
                  + '**4)** If you do not use it within 60 minutes, the key will expire and you will have to redeem a new one.\n'
                  + '**5)** To cancel a mention, type `?mention cancel`. \n'
                  + '**6)** Example: `?mention 484484358879707136 Japanese Grammar` \n'
                  + 'To get your message ID, follow: https://support.discordapp.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-'
                }*/
              ],
              timestamp: new Date(),
              footer: {
                icon_url: client.user.avatarURL,
                text: 'StudyBot'
              }
            }
          }
        );

      await message
        .delete();
    } catch (err) {
      Raven.captureException(err);
    }
  }
};
