const Raven = require('raven');
const fs = require('fs');
let rawdata = fs.readFileSync('countries.json');
var contries = JSON.parse(rawdata);

module.exports = {
  description: 'Role adding/removing in #change-role',
  async execute(message) {
    try {
      const {
        content,
        guild,
        member,
        author
      } = message;

      // await message.reply(contries[0].Name);

      const command = content.slice(0, 1).toLowerCase(); // get first part of string (command)
      let selectedRole = content.slice(1).trim().toLowerCase(); // get the rest of the string

      if (selectedRole.includes('-')) {
        selectedRole = selectedRole.replace(/-/g, ' '); // replace dash with space if contained.
      }

      // Run through roles and check stipulations
      const validRole = await Promise.all(guild.roles
        .map(async (role) => {
          const {
            id,
            name
          } = role;

          if (selectedRole === name.toLowerCase()) {
            if (command === '+'
              && selectedRole === name.toLowerCase()) {
              if (member.roles.has(id)) {
                await message
                  .reply(`error! You are already in the **${name}** role!`);
              } else {
                await guild.member(author.id)
                  .addRole(id);

                await message
                  .reply(`you have added the **${name}** role!`);
              }
            } else if (command === '-'
              && selectedRole === name.toLowerCase()) {
              if (member.roles.has(id)) {
                await guild.member(author.id)
                  .removeRole(id);

                await message
                  .reply(`you have removed the **${name}** role!`);
              } else {
                await message
                  .reply(`error! You are not in the **${name}** role!`);
              }
            }
            return true;
          }
          return false;
        }));
      if (!validRole.includes(true)) {
        const validCountry = await Promise.all(Object.keys(contries)
          .map(async function (role) {
            if ((selectedRole.toLowerCase() === contries[role].Name.toLowerCase() && command === '+')) {
                const newrole = await guild.createRole({
                  name: contries[role].Name,
                  color: 'BLUE'
                });
                await guild.member(author.id)
                  .addRole(newrole.id);
                await message
                  .reply(`you have added the **${contries[role].Name}** role!`);
              return true;
            }
          }));
      }
    } catch (err) {
      Raven.captureException(err);
    }
  }
};
